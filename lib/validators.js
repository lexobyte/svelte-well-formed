export const usernameRegex = /^\w+$/;
export const emailRegex = /^[\w.]+@[-\w]+\.[-\w.]+[\w]$/;
export const passwordsMustMatch = 'Passwords must match';

export const thisFieldIsRequired = 'This field is required.';
export const passwordIsTooShort = 'Password is too short.';


export const isRequired = value => {
	const valid = value !== '';
	return [valid, valid ? '' : thisFieldIsRequired];
};


export const isValidUsername = username => {
	const valid = usernameRegex.test(username);
	return [valid, valid ? '' : 'Invalid username.'];
};


export const isValidPassword = password => {
	if (password.length < 8) {
		return [false, passwordIsTooShort];
	}
	// Use toLowerCase, toUpperCase instead of regexes so we don't reject
	// passwords consisting of non-ASCII characters.
	if (password === password.toUpperCase()) {
		return [false, 'Password must contain a lowercase letter.'];
	}
	if (password === password.toLowerCase()) {
		return [false, 'Password must contain an uppercase letter.'];
	}
	if (!/[0-9]/.test(password)) {
		return [false, 'Password must contain a number.'];
	}
	if (/^[a-z][A-Z][0-9]$/.test(password)) {
		return [false, 'Password must contain a non-alphanumeric symbol.'];
	}
	return [true, ''];
};


export const isLaxPassword = password => {
	const valid = password.length >= 8;
	return [valid, valid ? '' : passwordIsTooShort];
};


export const isValidEmail = email => {
	const valid = emailRegex.test(email);
	return [valid, valid ? '' : 'Invalid email address.'];
};
