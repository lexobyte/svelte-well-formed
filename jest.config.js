module.exports = {
	transform: {
		'^.+\\.svelte$': 'svelte-jester',
		'^.+\\.js$': 'babel-jest',
	},
	moduleFileExtensions: ['js', 'svelte'],
	testMatch: [
		// "**/__tests__/**/*.[jt]s?(x)",
		"**/?(*.)+(spec|test).[jt]s?(x)"
	],
	setupFilesAfterEnv: ['./test/jest.setup.js'],
}
