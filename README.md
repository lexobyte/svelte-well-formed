# svelte-well-formed
Another form library for Svelte, svelte-well-formed aims to create forms that
are accessible and customisable, while also catering for common requirements
like validation.


## Installation
As usual, for a Svelte project:
```js
    npm install --save-dev svelte-well-formed
```

## Usage
Import the form component:
```javascript
import Form from 'svelte-well-formed';
```

Configure some fields and define a submit handler:
```javascript
const fields = [
    {
        name: 'username',
        label: 'Username',
    },
    {
        name: 'password',
        label: 'Password',
    },
];

const handleSubmit = async values => {
    // Do something asynchronously here.
    // Or just log values.
    console.log('Username:', values.username);
    console.log('Password:', values.password);
};
```

Use!
```svelte
<Form id="login" {fields} onSubmit={handleSubmit}/>
```


## Client-side validation
Client-side validators can be added to a field under the `validators` property.
Each validator must be a function with signature
```typescript
(value: string) => [boolean /* Value is valid */, string /* Error message */]
```

The returned pair contains a boolean, indicating whether the input was valid;
and an error message. If valid, then the error message will be ignored. If
invalid and the error message is falsy, a default message will be substituted.
```javascript
const myValidator = value => [value.length >= 8, 'Password is too short.'];
const fields = [
    {
        name: 'password',
        label: 'Password',
        validators: [myValidator],
    },
    // ...
];
```

Validators will be checked in the order they are given in. The first validator
to fail dictates what error will be displayed.

### Built-in validators
The library comes with a number of built-in validators. For example:
```javascript
import {isRequired, isValidPassword} from 'svelte-well-formed/lib/validators';

field.validators = [isRequired, isValidPassword];
```

See [./lib/validators.js](lib/validators.js) for more available validators.


## Server-side validation
Of course, client-side validation is only half of the problem. Most of the time
we also need to validate on the server. You can indicate the success or failure
of a form submission with the return value (or returned promise's resolved
value) of the `onSubmit` handler.

If everything succeeded, the submit handler needn't return anything:

```javascript
const onSubmit = values => undefined;
```

To indicate that field-specific errors were encountered:

```javascript
const onSubmit = async values => {
    // ... do stuff ...
    return {
        errors: {
            username: 'A user with that username already exists.',
        },
    };
};
```

Errors which are not field-specific can be returned as well/instead, using the
`formError` property:

```javascript
const onSubmit = async values => {
    // ... do stuff ...
    return {
        errors: {
            // ...
        },
        formError: 'The provided username or password was incorrect.',
    };
};
```

Finally, if your submit handler throws an error or returns a promise which
rejects, the form will act as if you returned a generic `formError`:

```javascript
const onSubmit = values => {
    throw new Error('Uh oh!');
};
```

We don't recommend (intentionally) using this final approach; it's merely there
to protect your UI from an unexpected failure.
