import {
	isRequired,
	isValidUsername,
	isValidPassword,
	isLaxPassword,
	isValidEmail,
} from '../lib/validators';


describe('isRequired', () => {
	it('regards falsy values as invalid', () => {
		const [valid, message] = isRequired('');
		expect(valid).toBe(false);
		expect(message).toBeTruthy();
	});
	it('regards truthy values as valid', () => {
		const [valid, message] = isRequired('a truthy value');
		expect(valid).toBe(true);
		expect(message).toBeFalsy();
	});
});


describe('isValidUsername', () => {
	it('permits values including only alphanumeric or "_" characters', () => {
		const [valid, message] = isValidUsername('123_abc');
		expect(valid).toBe(true);
		expect(message).toBeFalsy();
	});
	it('rejects values including inadmissable characters', () => {
		for (const username of [
			'123-abc',
			'123abc!',
			'123@abc',
			'123.abc',
		]) {
			const [valid, message] = isValidUsername(username);
			expect(valid).toBe(false);
			expect(message).toBeTruthy();
		}
	});
});


describe('isValidPassword', () => {
	it('rejects values that are too short', () => {
		const [valid, message] = isValidPassword('aA1!');
		expect(valid).toBe(false);
		expect(message).toBe('Password is too short.');
	});
	it('rejects values that have no numbers', () => {
		const [valid, message] = isValidPassword('aA!bcdef');
		expect(valid).toBe(false);
		expect(message).toBe('Password must contain a number.');
	});
	it('rejects values that have no lowercase letters', () => {
		const [valid, message] = isValidPassword('PASSWORD1!');
		expect(valid).toBe(false);
		expect(message).toBe('Password must contain a lowercase letter.');
	});
	it('rejects values that have no uppercase letters', () => {
		const [valid, message] = isValidPassword('password1!');
		expect(valid).toBe(false);
		expect(message).toBe('Password must contain an uppercase letter.');
	});
	it('accepts other values', () => {
		const [valid, message] = isValidPassword('p45sW@RD');
		expect(valid).toBe(true);
		expect(message).toBe('');
	});
});


describe('isLaxPassword', () => {
	it('rejects values that are too short', () => {
		const [valid, message] = isLaxPassword('pswd');
		expect(valid).toBe(false);
		expect(message).toBe('Password is too short.');
	});
	it('accepts values of length at least 8', () => {
		const [valid, message] = isLaxPassword('password');
		expect(valid).toBe(true);
		expect(message).toBe('');
	});
});


describe('isValidEmail', () => {
	for (const [testname, email] of [
		["Missing '@' symbol", 'email-at-domain.com'],
		["Missing TLD", 'email@domain'],
		["Missing username", '@domain.com'],
	]) {
		it(testname, () => {
			const [valid, message] = isValidEmail(email);
			expect(valid).toBe(false);
			expect(message).toBe('Invalid email address.');
		});
	}
	it('accepts valid emails', () => {
			const [valid, message] = isValidEmail('email@example.com');
			expect(valid).toBe(true);
			expect(message).toBe('');
	});
});
