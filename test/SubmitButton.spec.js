import {render} from '@testing-library/svelte'

import SubmitButton from '../lib/SubmitButton.svelte'


// See https://testing-library.com/docs/svelte-testing-library/api/#component-options and https://svelte.dev/docs#Client-side_component_API for details of modifying a component via Svelte's client-side API for the sake of testing.
// Also remember to tick() your component after changes, if using $set.


it('contains "Submit" text by default', () => {
	const {queryByText} = render(SubmitButton);
	const button = queryByText('Submit');
	expect(button).not.toBeNull();
});

it('contains the text provided, if any', async () => {
	const {queryByText} = render(SubmitButton, {
		text: 'Custom text',
	})
	const button = queryByText('Custom text');
	expect(button).not.toBeNull();
})

it('has a button, with "submitting" class when submitting', () => {
	const {queryByRole} = render(SubmitButton, {
		submitting: true,
	});
	const button = queryByRole('button');
	expect(button).toHaveClass('submitting');
});
