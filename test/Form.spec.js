import {tick} from 'svelte';
import {screen, render, fireEvent} from '@testing-library/svelte';
import userEvent from '@testing-library/user-event';

import Form from '../index.svelte';


const defaultProps = Object.freeze({
	id: 'test-form',
	fields: Object.freeze([
		Object.freeze({
			name: 'library',
			label: 'Library',
			autocomplete: 'off',
		}),
	]),
	onSubmit: () => {},
	'aria-label': 'a testing form',
});


it('renders a form', () => {
	render(Form, defaultProps);
	const elem = screen.queryByRole('form');
	expect(elem).not.toBe(null);
});

it('contains an appropriate input element for each field', () => {
	const fields = [
		{
			name: 'text',
			type: 'text',
			label: 'Text',
			autocomplete: 'off',
		},
		{
			name: 'number',
			type: 'number',
			label: 'Number',
			autocomplete: 'off',
		},
		{
			name: 'textarea',
			type: 'textarea',
			label: 'Textarea',
			autocomplete: 'off',
		},
	];

	render(Form, {
		...defaultProps,
		fields,
	});

	expect(screen.getByLabelText('Text')).toBeInTheDocument();
	expect(screen.getByLabelText('Number')).toBeInTheDocument();
	expect(screen.getByLabelText('Textarea')).toBeInTheDocument();
});

it('contains an accessible input element for each field', () => {
	const fields = [
		{
			name: 'username',
			type: 'text',
			label: 'Username',
			title: 'Username',
			autocomplete: 'username',
			placeholder: 'Username',
		},
		{
			name: 'password',
			type: 'password',
			label: 'Password',
			title: 'Password',
			autocomplete: 'current-password',
			placeholder: 'Password',
		},
		{
			name: 'message',
			type: 'textarea',
			label: 'Message',
			title: 'Message',
			autocomplete: 'off',
			placeholder: 'Message',
		},
		{
			name: 'checkbox',
			type: 'checkbox',
			label: 'Checkbox',
			autocomplete: 'off',
		},
	];

	render(Form, {
		...defaultProps,
		fields,
	});

	const username = screen.getByLabelText('Username');
	const password = screen.getByLabelText('Password');
	const message = screen.getByLabelText('Message');
	const checkbox = screen.getByLabelText('Checkbox');

	expect(username).toBeInTheDocument();
	expect(password).toBeInTheDocument();
	expect(message).toBeInTheDocument();
	expect(checkbox).toBeInTheDocument();

	expect(username).toBe(screen.getByRole('textbox', {name: 'Username'}));
	// According to https://www.w3.org/TR/html-aria/, <input type="password"> has
	// no corresponding role.
	expect(message).toBe(screen.getByRole('textbox', {name: 'Message'}));
	expect(checkbox).toBe(screen.getByRole('checkbox', {name: 'Checkbox'}));
});


it('contains a submit button', () => {
	render(Form, defaultProps);
	expect(screen.getByRole('button')).toBeInTheDocument();
});

it('uses the values provided in the values object', () => {
	const {component} = render(Form, {
		...defaultProps,
		values: {
			library: 'Svelte',
		},
	});
	expect(component.values.library).toBe('Svelte');
	expect(screen.getByRole('textbox').value).toBe('Svelte');
});

it('can be updated via the values object', () => {
	const values = {
		library: 'Svelte',
	};
	const {component} = render(Form, {
		...defaultProps,
		values,
	});
	expect(component.values.library).toBe('Svelte');
	component.values = {
		library: 'React',
	};
	expect(component.values.library).toBe('React');
	expect(screen.getByRole('textbox').value).toBe('React');
});

it('disables the submit button when submitting prop is set', () => {
	render(Form, {
		...defaultProps,
		submitting: true,
	});
	expect(screen.getByRole('button')).toBeDisabled();
});


describe('submission', () => {
	const createSubmitter = () => {
		const out = {};
		out.promise = new Promise((resolve, reject) => {
			out.resolve = resolve;
			out.reject = reject;
		});
		out.onSubmit = () => out.promise;
		return out;
	};

	it('calls the onSubmit() handler with its values on submit', () => {
		const onSubmit = jest.fn(() => null);
		render(Form, {
			...defaultProps,
			fields: [
				{
					name: 'language',
					label: 'Language',
					autocomplete: 'off',
				},
				{
					name: 'library',
					label: 'Library',
					autocomplete: 'off',
				},
			],
			values: {
				language: 'ECMAScript',
				library: 'Svelte',
			},
			onSubmit,
		});
		fireEvent.click(screen.getByRole('button'));
		expect(onSubmit).toHaveBeenCalledTimes(1);
		expect(onSubmit).toHaveBeenCalledWith({
			language: 'ECMAScript',
			library: 'Svelte',
		});
	});

	it('calls onSubmit() with changed values on submit', async () => {
		const onSubmit = jest.fn(() => null);
		const {component} = render(Form, {
			...defaultProps,
			fields: [
				{
					name: 'username',
					label: 'Username',
					autocomplete: 'username',
					required: true,
				},
				{
					name: 'password',
					label: 'Password',
					autocomplete: 'current-password',
					required: true,
				},
			],
			onSubmit,
		});
		const usernameInput = screen.getByLabelText('Username');
		const passwordInput = screen.getByLabelText('Password');
		userEvent.type(usernameInput, 'testuser');
		userEvent.type(passwordInput, 'p4S#w0rD');
		expect(component.values).toEqual({
			username: 'testuser',
			password: 'p4S#w0rD',
		});
		userEvent.click(screen.getByRole('button'));
		expect(onSubmit).toHaveBeenCalledTimes(1);
		expect(onSubmit).toHaveBeenCalledWith({
			username: 'testuser',
			password: 'p4S#w0rD',
		});
	});

	it('sets the submitting prop on submit button click', () => {
		const {component} = render(Form, {
			...defaultProps,
			onSubmit: () => new Promise(() => {}),
		});
		fireEvent.click(screen.getByRole('button'));
		expect(component.submitting).toBe(true);
	});

	it('disables the submit button on click', async () => {
		render(Form, {
			...defaultProps,
			onSubmit: () => new Promise(() => {}),
		});
		const button = screen.getByRole('button');
		fireEvent.click(button);
		await tick();
		expect(button).toBeDisabled();
	});

	for (const method of ['resolve', 'reject']) {
		it(`unsets the submitting prop when onSubmit() ${method}s`, async () => {
			const submitter = createSubmitter();
			const {component} = render(Form, {
				...defaultProps,
				onSubmit: submitter.onSubmit,
			});
			fireEvent.submit(screen.getByRole('form'));
			// A sanity check. It duplicates a test above, but also ensures this one
			// is actually testing something.
			expect(component.submitting).toBe(true);
			submitter[method](null);
			await tick();
			expect(component.submitting).toBe(false);
		});
	}

	it('re-enables the button when onSubmit() succeeds', async () => {
		const submitter = createSubmitter();
		render(Form, {
			...defaultProps,
			onSubmit: submitter.onSubmit,
		});
		const button = screen.getByRole('button');

		fireEvent.submit(screen.getByRole('form'));
		await tick();
		expect(button).toBeDisabled();

		submitter.resolve(undefined);
		await tick();
		expect(button).not.toBeDisabled();
	});

	it('disables submit after failures, until a value changes', async () => {
		render(Form, {
			...defaultProps,
			onSubmit: () => Promise.resolve({
				errors: {
					library: 'This field was invalid.',
				},
			}),
		});
		fireEvent.submit(screen.getByRole('form'));

		const button = screen.getByRole('button');
		await tick();
		expect(button).toBeDisabled();

		const input = screen.getByRole('textbox');
		input.value = 'Svelte3';
		fireEvent.change(input, {bubbles: true});

		await tick();
		expect(button).not.toBeDisabled();
	});

	it('sets succeeded when onSubmit() resolves without errors', async () => {
		const submitter = createSubmitter();
		const {component} = render(Form, {
			...defaultProps,
			onSubmit: submitter.onSubmit,
		});
		fireEvent.submit(screen.getByRole('form'));
		submitter.resolve({});
		await tick();
		expect(component.succeeded).toBe(true);
	});

	it('sets succeeded to false when onSubmit() rejects', async () => {
		const submitter = createSubmitter();
		const {component} = render(Form, {
			...defaultProps,
			onSubmit: submitter.onSubmit,
		});
		fireEvent.submit(screen.getByRole('form'));
		submitter.reject();
		await tick();
		expect(component.succeeded).toBe(false);
	});

	it('sets succeeded when onSubmit() resolves with errors', async () => {
		const {component} = render(Form, {
			...defaultProps,
			onSubmit: () => Promise.resolve({
				errors: {
					library: 'This value is not permitted.',
				},
			}),
		});
		fireEvent.submit(screen.getByRole('form'));
		await tick();
		expect(component.succeeded).toBe(false);
	});

	it('shows error messages when onSubmit() resolves with them', async () => {
		render(Form, {
			...defaultProps,
			onSubmit: () => Promise.resolve({
				errors: {
					library: 'This value is not permitted.',
				},
			}),
		});
		fireEvent.submit(screen.getByRole('form'));
		const message = await screen.findByRole('alert');
		expect(message).toBeInTheDocument();
		expect(message).toHaveTextContent('This value is not permitted.');
	});

	it('sets a generic error when onSubmit() rejects', async () => {
		render(Form, {
			...defaultProps,
			onSubmit: () => Promise.reject(),
		});
		fireEvent.submit(screen.getByRole('form'));
		const alert = await screen.findByRole('alert');
		expect(alert).toBeInTheDocument();
		expect(alert).toHaveTextContent('An unexpected error occurred.');
	});
});
