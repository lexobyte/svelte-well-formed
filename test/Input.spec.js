import {tick} from 'svelte';
import {render, screen, fireEvent} from '@testing-library/svelte'
import userEvent from '@testing-library/user-event';
import {v4 as uuid} from 'uuid';

import Input from '../lib/Input.svelte'


const defaultOptions = Object.freeze({
	id: 'input-test-elem',
	name: 'food',
	label: 'Food',
	autocomplete: 'off',
});


const alwaysInvalidValidator = () => [false, 'The input is bad.'];
const alwaysValidValidator = () => [true, ''];


const testWith = ({type, tagName, role}) => {
	const tagLower = tagName.toLowerCase();
	const isCheckbox = type === 'checkbox';
	const anAppropriateValue = type === 'checkbox' ? true : 'a value';
	const anotherValue = type === 'checkbox' ? false : 'another generic value';

	describe(`the component (type=${type})`, () => {
		it(`contains a labelled <${tagLower}>`, () => {
			render(Input, {
				...defaultOptions,
				type,
			});
			const elem = screen.queryByLabelText(defaultOptions.label);
			expect(elem).toBeInTheDocument();
			expect(elem.tagName).toBe(tagName);
		});
		it(`adorns the ${tagLower} element with the given class`, () => {
			render(Input, {
				...defaultOptions,
				type,
				cls: 'custom-class',
			});
			const elem = screen.queryByLabelText(defaultOptions.label);
			expect(elem).toHaveClass('custom-class');
		});
		it('is wrapped in a .group element with role="group"', () => {
			render(Input, {
				...defaultOptions,
				type,
			});
			const group = screen.queryByRole('group');
			expect(group).toBeInTheDocument();
		});
		it('uses the `groupCls` prop for the group element class', () => {
			render(Input, {
				...defaultOptions,
				type,
				groupCls: 'custom-group-class',
			});
			const group = screen.queryByRole('group');
			expect(group).toHaveClass('custom-group-class');
		});
		it('uses the placeholder prop', () => {
			render(Input, {
				...defaultOptions,
				type,
				placeholder: 'Cookies',
			});
			const elem = screen.getByRole(role);
			expect(elem.placeholder).toBe('Cookies');
		});

		it('updates the value prop on input', () => {
			const {component} = render(Input, {
				...defaultOptions,
				type,
				value: isCheckbox ? true : 'the value',
			});
			const elem = screen.getByRole(role);
			if (isCheckbox) {
				elem.checked = false;
				fireEvent.input(elem);
				expect(component.value).toBe(false);
			}
			else {
				userEvent.type(elem, ' changed');
				expect(component.value).toBe('the value changed');
			}
		});

		it('updates the input on value prop change', async () => {
			const {component} = render(Input, {
				...defaultOptions,
				type,
				value: isCheckbox ? true : 'the value',
			});
			component.value = isCheckbox ? false : 'changed';
			await tick();
			const elem = screen.getByRole(role);
			if (isCheckbox) {
				expect(elem).not.toBeChecked();
			}
			else {
				expect(elem).toHaveValue('changed');
			}
		});

		const checkEventBehaviour = (eventType, {alertIfInvalid}) => {
			it('validates', () => {
				const mockValidator = jest.fn(alwaysValidValidator);
				render(Input, {
					...defaultOptions,
					type,
					validators: [mockValidator],
					value: anAppropriateValue,
				});
				const elem = screen.getByRole(role);
				fireEvent[eventType](elem);
				expect(mockValidator).toHaveBeenCalledTimes(1);

				expect(mockValidator).toHaveBeenCalledWith(anAppropriateValue);
			});

			it('does not display an error on valid input', async () => {
				render(Input, {
					...defaultOptions,
					type,
					validators: [() => [true, 'validation message']],
				});
				const elem = screen.getByRole(role);
				fireEvent[eventType](elem);
				await tick();
				expect(screen.queryByText('validation message')).toBe(null);
			});

			// We always want to display a visual error if input is incorrect.
			it('displays a visual error if validation fails', async () => {
				render(Input, {
					...defaultOptions,
					type,
					validators: [alwaysInvalidValidator],
				});
				fireEvent[eventType](screen.getByRole(role));
				await tick();
				const errorText = alwaysInvalidValidator()[1];
				const errorElem = screen.queryByText(errorText);
				expect(errorElem).toBeInTheDocument();
			});

			// The event type affects whether we want the visual error to be an alert
			// (e.g. role="alert"), as this can disrupt the user.
			const alertTestName = alertIfInvalid
				?	'alerts on invalid input'
				: 'does not alert on invalid input';
			it(alertTestName, async () => {
				render(Input, {
					...defaultOptions,
					type,
					validators: [alwaysInvalidValidator],
				});
				fireEvent[eventType](screen.getByRole(role));
				await tick();
				const elem = screen.queryByRole('alert');
				if (alertIfInvalid) {
					expect(elem).toBeInTheDocument();
					const input = screen.getByRole(isCheckbox ? 'checkbox' : 'textbox');
					expect(input).toBeInvalid();
				}
				else {
					expect(elem).toBe(null);
				}
			});
		};

		describe('on input events', () => {
			// We don't want to alert on invalid values after an input event. This
			// would be disruptive to users.
			checkEventBehaviour('input', {
				alertIfInvalid: false,
			});
		});

		describe('on change events', () => {
			// We do want to alert on invalid values after an input event. This
			// should be disruptive.
			checkEventBehaviour('change', {
				alertIfInvalid: true,
			});
		});
	});
};


testWith({tagName: 'INPUT', type: 'text', role: 'textbox'});
testWith({tagName: 'INPUT', type: 'checkbox', role: 'checkbox'});
testWith({tagName: 'TEXTAREA', type: 'textarea', role: 'textbox'});
