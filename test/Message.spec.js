import {render} from '@testing-library/svelte'

import Message from '../lib/Message.svelte';


it('is visible if `hidden` is falsy', () => {
	const {queryByText} = render(Message, {
		id: 'message',
		hidden: false,
		text: 'Something important',
	});
	const elem = queryByText('Something important');
	expect(elem).toBeInTheDocument();
	expect(elem).toBeVisible();
});

it('is not rendered if `hidden` is set', () => {
	const {queryByText} = render(Message, {
		id: 'message',
		hidden: true,
		text: 'Something important',
	});
	const elem = queryByText('Something important');
	expect(elem).toBe(null);
});

it('is not visible if `text` is falsy', () => {
	const {queryByText} = render(Message, {
		id: 'message',
	});
	const elem = queryByText('Something important');
	expect(elem).toBe(null);
});
